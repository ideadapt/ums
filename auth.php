<?php
session_start();

define('USERNAME', 'a name');
define('PASSWORD', 'b stupid password');

function login($reqUname, $reqPwd){
	if($reqUname === USERNAME && $reqPwd === PASSWORD){
		$_SESSION['uname'] = $reqUname;
	}else{
		http_response_code(400);
	}
}
function status(){
	return isset($_SESSION['uname']) && $_SESSION['uname'] === USERNAME ? 'logged_in' : 'logged_out';
}

$reqUname = filter_input(INPUT_POST, 'uname', FILTER_SANITIZE_STRING);
$reqPwd = filter_input(INPUT_POST, 'pwd', FILTER_SANITIZE_STRING);

if(isset($_GET['logout'])){
	session_destroy();
}else if(isset($reqUname) && isset($reqPwd)){
	login($reqUname, $reqPwd);
}else{
	echo status();
}
?>
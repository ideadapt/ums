<?php
session_start();

function update($id, $content){
	$filename = "data.json";
	fopen($filename, "a") or die("can not write db file");

	$contents = file_get_contents($filename);
	$contents = json_decode($contents, true);
	if(!$contents){
		$contents = array();
	}
	$contents[$id] = $content;

	file_put_contents($filename, json_encode($contents));
}

function get($id){
	$filename = "data.json";
	fopen($filename, "r") or die("can not read db file");

	$allowed_tags = '<a><i><span><h1><h2><div><strong><b><p><img><article><address>';
	$contents = file_get_contents($filename);
	$contents = json_decode($contents, true);
	$content = $contents[$id];
	$content = strip_tags($content, $allowed_tags);
	return $content;
}

$id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_STRING);
$ids = filter_var_array(explode(',', $_GET['ids']), FILTER_SANITIZE_STRING);

if(!empty($id)){
	echo get($id);
}elseif(!empty($ids[0])){
	$blocks = array();
	foreach($ids as $id){
		$blocks[$id] = get($id);
	}
	echo json_encode($blocks);
}elseif(isset($_SESSION['uname']) && isset($_POST['blocks'])){

	$blocks = $_POST['blocks'];
	$blocks = json_decode($blocks, true);

	foreach($blocks as $k=>$block){
		echo update($block['id'], $block['content']);
	}
}else{
	http_response_code(400);
}
?>